# C Preparatory Assignments

## Evaluation
* Today create private GIT repository for all C Preparatory Assignments.
* Add your case study evaluator as Developer member into that repository.
* Push all completed assignments to the GIT repository.
* Try to complete remaining assignments on Today to Saturday and keep uploading in GIT.
* All assignments will be evaluated on Saturday/Sunday.

## Lecture Plan
* Assignment 8:
	* strtok() -- Assignment 1
	* struct
	* Singly Linked List --
	* Read text file line by line -- fgets()

* Assignment 4

* Assignment 6

## Team work
